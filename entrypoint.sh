#!/bin/sh

# Exit immediately if a command exits with a non-zero status.
set -e

# Substitute all ${} to Environment Variables values and put result to the file default.conf
envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf
# Start nginx in foreground - so we will get all logs getting to standard output.
nginx -g 'daemon off;'