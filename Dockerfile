FROM nginxinc/nginx-unprivileged:1-alpine
LABEL maintainer="maintainer@yahoo.com"

COPY ./default.conf.tpl /etc/nginx/default.conf.tpl
COPY ./uwsgi_params /etc/nginx/uwsgi_params

ENV LISTEN_PORT=8000
ENV APP_HOST=app
ENV APP_PORT=9000

USER root

# Creat new directory with subdirectories
RUN mkdir -p /vol/static
# Setting permissions for this directory
RUN chmod 755 /vol/static
# Create empty file for envsubst
RUN touch /etc/nginx/default.conf
# Change ownership of this new file to group:nginx and user:nginx
RUN chown nginx:nginx /etc/nginx/default.conf

COPY ./entrypoint.sh /entrypoint.sh
# cmod +x - telling that this is executable file.
RUN chmod +x /entrypoint.sh

USER nginx

CMD ["/entrypoint.sh"]